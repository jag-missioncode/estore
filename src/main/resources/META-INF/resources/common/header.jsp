<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">DigiStore</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-link active" href="home">Home <span class="sr-only">(current)</span></a>
            <a class="nav-link" href="products">Products</a>
            <a class="nav-link" href="offers">Offers</a>
            <a class="nav-link" href="faq">Faq</a>
        </div>
    </div>
</nav>