package com.frontend.estore;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

@Controller
@Slf4j
public class HomeController {

    @GetMapping("home")
    public String landingPage(Model model){
        log.info("Home Controller resolve home.jsp");
        model.addAttribute("appname", "DigiStore");
        model.addAttribute("products", Arrays.asList("Laptop", "Monitor","Mousepad", "Standing Desk"));
        model.addAttribute("copyrightyear", Calendar.getInstance().get(Calendar.YEAR));
        return "home";
    }

    @GetMapping("faq")
    public String faq(){
        return "faq";
    }
}
