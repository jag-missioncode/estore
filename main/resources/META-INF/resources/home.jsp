<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="common/head.jsp"/>
<body>
<jsp:include page="common/header.jsp"/>

<h3>Product Catalog</h3>
<div class="col-md-4">
    <ul class="list-group">
        <c:forEach items="${products}" var="product">
            <li class="list-group-item">${product}</li>
        </c:forEach>
    </ul>
</div>
<jsp:include page="common/footer.jsp"/>
</body>
</html>